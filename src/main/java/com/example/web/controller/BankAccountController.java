package com.example.web.controller;

import com.example.web.entity.BankAccountNoEmployee;
import com.example.web.entity.table.BankAccount;
import com.example.web.entity.table.Employee;
import com.example.web.helper.MapBankAccountNoEmployeeToEntity;
import com.example.web.repository.bankaccount.BankAccountDao;
import com.example.web.repository.employee.EmployeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/v1/bank-account")
public class BankAccountController {
    @Autowired
    private BankAccountDao bankAccountDao;

    @Autowired
    private EmployeeDao employeeDao;

    @GetMapping("/{employeeId}")
    public ResponseEntity<Object> retrieveBankAccountsByEmployeeId(@PathVariable Long employeeId) {
        List<BankAccount> bankAccount = bankAccountDao.findByEmployeeIdAndDeletedAtIsNull(employeeId);

        Map<String, Object> res = new HashMap<>();
        res.put("message", "success");
        res.put("results", bankAccount);

        return ResponseEntity.ok(res);
    }

    @GetMapping("/{employeeId}/{bankAccountId}")
    public ResponseEntity<Object> retrieveBankAccountByEmployee(@PathVariable Long employeeId, @PathVariable Long bankAccountId) {
        Optional<BankAccount> bankAccountOptional = bankAccountDao.findOneByIdAndEmployeeIdAndDeletedAtIsNull(bankAccountId, employeeId);

        if (!bankAccountOptional.isPresent()) return ResponseEntity.notFound().build();

        Map<String, Object> res = new HashMap<>();
        res.put("message", "success");
        res.put("result", bankAccountOptional.get());

        return ResponseEntity.ok(res);
    }

    @PostMapping("/{employeeId}")
    public ResponseEntity<Object> addBankAccount(@PathVariable Long employeeId, @RequestBody BankAccountNoEmployee bankAccountReq) {
        MapBankAccountNoEmployeeToEntity mapper = MapBankAccountNoEmployeeToEntity.getInstance();
        BankAccount bankAccount = mapper.map(bankAccountReq, employeeDao.getById(employeeId));
        BankAccount savedBankAccount = bankAccountDao.save(bankAccount);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{employeeId}/{bankAccountId}").buildAndExpand(employeeId, savedBankAccount.getId()).toUri();

        Map<String, Object> res = new HashMap<>();
        res.put("message", "success");
        res.put("result", savedBankAccount);

        return ResponseEntity.created(location).body(res);
    }

    @PutMapping("/{employeeId}/{bankAccountId}")
    public ResponseEntity<Object> updateBankAccount(@PathVariable Long employeeId, @PathVariable Long bankAccountId, @RequestBody BankAccountNoEmployee bankAccountReq) {
        MapBankAccountNoEmployeeToEntity mapper = MapBankAccountNoEmployeeToEntity.getInstance();
        Optional<Employee> employeeOptional = employeeDao.findById(employeeId);
        Optional<BankAccount> bankAccountOptional = bankAccountDao.findById(bankAccountId);

        if (!employeeOptional.isPresent() || !bankAccountOptional.isPresent()) return ResponseEntity.notFound().build();

        BankAccount bankAccount = mapper.map(bankAccountReq, employeeOptional.get());
        bankAccount.setId(bankAccountId);
        BankAccount updatedBankAccount = bankAccountDao.save(bankAccount);

        Map<String, Object> res = new HashMap<>();
        res.put("message", "success");
        res.put("result", updatedBankAccount);

        return ResponseEntity.ok(res);
    }

    @DeleteMapping("/{employeeId}/{bankAccountId}")
    public ResponseEntity<Object> deleteBankAccount(@PathVariable Long employeeId, @PathVariable Long bankAccountId) {
        bankAccountDao.softDeleteById(employeeId, bankAccountId);

        Map<String, Object> res = new HashMap<>();
        res.put("message", "success");

        return ResponseEntity.ok(res);
    }
}
