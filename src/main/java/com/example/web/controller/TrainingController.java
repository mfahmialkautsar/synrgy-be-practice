package com.example.web.controller;

import com.example.web.entity.table.EmployeeTraining;
import com.example.web.entity.table.Training;
import com.example.web.repository.employeetraining.EmployeeTrainingDataSource;
import com.example.web.repository.training.TrainingDao;
import com.example.web.repository.training.TrainingDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/v1/training")
public class TrainingController {

    @Autowired
    private TrainingDataSource trainingDataSource;

    @PostMapping("")
    public ResponseEntity<Object> addTraining(@RequestBody Training training) {
        Training savedTraining = trainingDataSource.insert(training);

        Map<String, Object> res = new HashMap<>();
        res.put("status", "success");
        res.put("result", savedTraining);

        return new ResponseEntity<>(res, HttpStatus.CREATED);
    }

    @GetMapping("")
    public ResponseEntity<Object> retrieveTrainings(@RequestParam(required = false) Integer page) {
        Page<Training> trainings = trainingDataSource.getAll(page);

        Map<String, Object> res = new HashMap<>();
        res.put("status", "success");
        res.put("page", trainings.getNumber());
        res.put("total_results", trainings.getTotalElements());
        res.put("total_pages", trainings.getTotalPages());
        res.put("results", trainings.getContent());

        return ResponseEntity.ok(res);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> retrieveTraining(@PathVariable Long id) {
        Optional<Training> training = trainingDataSource.getById(id);

        if (!training.isPresent()) return ResponseEntity.notFound().build();

        Map<String, Object> res = new HashMap<>();
        res.put("status", "success");
        res.put("result", training.get());

        return ResponseEntity.ok(res);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateTraining(@RequestBody Training training, @PathVariable Long id) {
        Map<String, Object> res = new HashMap<>();
        try {
            trainingDataSource.update(training, id);
            res.put("status", "success");
        } catch (Exception e) {
            res.put("status", "fail");
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }

        return ResponseEntity.ok(res);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteTraining(@PathVariable Long id) {
        Map<String, Object> res = new HashMap<>();
        try {
            trainingDataSource.delete(id);
            res.put("status", "success");
        } catch (Exception e) {
            res.put("status", "fail");
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }

        return ResponseEntity.ok(res);
    }
}
