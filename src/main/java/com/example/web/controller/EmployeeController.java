package com.example.web.controller;

import com.example.web.entity.table.Employee;
import com.example.web.entity.table.EmployeeDetail;
import com.example.web.repository.employee.EmployeeDataSource;
import com.example.web.repository.employee.EmployeeDetailDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1/employee")
public class EmployeeController {

    @Autowired
    private EmployeeDataSource employeeDataSource;
    @Autowired
    private EmployeeDetailDataSource employeeDetailDataSource;

    @GetMapping("")
    public ResponseEntity<Object> retrieveEmployees(@RequestParam(required = false, defaultValue = "1") Integer page, @RequestParam(required = false, defaultValue = "") String name, @RequestParam(required = false, defaultValue = "1") Integer status) {
        Map<String, Object> res = new HashMap<>();
        Page<Employee> pageableEmployee = employeeDataSource.getAll(page, name, status);

        res.put("status", "success");
        res.put("page", page);
        res.put("results", pageableEmployee.getContent());
        res.put("total_results", pageableEmployee.getTotalElements());
        res.put("total_pages", pageableEmployee.getTotalPages());

        return ResponseEntity.ok(res);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> retrieveEmployee(@PathVariable Long id) {
        Map<String, Object> res = new HashMap<>();
        try {
            Employee employee = employeeDataSource.getById(id);
            res.put("status", "success");
            res.put("result", employee);
            return ResponseEntity.ok(res);
        } catch (Exception e) {
            res.put("status", "fail");
            res.put("message", e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(res);
        }
    }

    @PostMapping("")
    public ResponseEntity<Object> addEmployee(@RequestBody Employee employee) {
        Employee savedEmployee = employeeDataSource.insert(employee);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedEmployee.getId()).toUri();

        Map<String, Object> res = new HashMap<>();
        res.put("status", "success");
        res.put("result", savedEmployee);

        return ResponseEntity.created(location).body(res);
    }

    @PostMapping("/{id}/detail")
    public ResponseEntity<Object> addEmployeeDetail(@RequestBody EmployeeDetail employeeDetail, @PathVariable Long id) {
        Map<String, Object> res = new HashMap<>();
        try {
            EmployeeDetail savedEmployeeDetail = employeeDetailDataSource.insert(employeeDetail, id);

            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}/detail").buildAndExpand(savedEmployeeDetail.getId()).toUri();

            res.put("status", "success");
            res.put("result", savedEmployeeDetail);

            return ResponseEntity.created(location).body(res);
        } catch (Exception e) {
            res.put("message", "detail is not empty");
            return ResponseEntity.badRequest().body(res);
        }
    }

    @PutMapping("/{id}/detail")
    public ResponseEntity<Object> updateEmployeeDetail(@RequestBody EmployeeDetail employeeDetail, @PathVariable Long id) {
        Map<String, Object> res = new HashMap<>();
        try {
            EmployeeDetail employeeDetailOptional = employeeDetailDataSource.update(employeeDetail, id);
            res.put("status", "success");
            res.put("result", employeeDetail);
            return ResponseEntity.ok(res);
        } catch (Exception e) {
            res.put("status", "fail");
            res.put("message", e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(res);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateEmployee(@RequestBody Employee employee, @PathVariable Long id) {
        Map<String, Object> res = new HashMap<>();
        try {
            Employee updatedEmployee = employeeDataSource.update(employee, id);
            res.put("status", "success");
            res.put("result", updatedEmployee);
            return ResponseEntity.ok(res);
        } catch (Exception e) {
            res.put("status", "fail");
            res.put("message", e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(res);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteEmployee(@PathVariable Long id) {
        Map<String, Object> res = new HashMap<>();
        try {
            res.put("status", "success");
            return ResponseEntity.ok(res);
        } catch (Exception e) {
            res.put("status", "fail");
            res.put("message", e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(res);
        }
    }
}
