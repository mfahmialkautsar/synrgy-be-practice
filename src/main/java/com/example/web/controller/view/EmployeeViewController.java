package com.example.web.controller.view;

import com.example.web.entity.table.Employee;
import com.example.web.repository.employee.EmployeeDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/employee")
public class EmployeeViewController {

    @Autowired
    private EmployeeDataSource employeeDataSource;

    @GetMapping("")
    public String index(Model model, @RequestParam(value = "page", defaultValue = "1") int page, @RequestParam(value = "name", defaultValue = "") String name, @RequestParam(value = "status", defaultValue = "1") int status) {
        model.addAttribute("title", "Employee");
        Page<Employee> employees = employeeDataSource.getAll(page, name, status);

        model.addAttribute("employee", employees.getContent());
        model.addAttribute("page", employees.getPageable().next().getPageNumber());
        model.addAttribute("prev", employees.getPageable().getPageNumber());
        model.addAttribute("next", employees.getPageable().next().getPageNumber() < employees.getTotalPages() ? employees.nextPageable().next().getPageNumber() : null);
        model.addAttribute("total_results", employees.getTotalElements());
        model.addAttribute("total_pages", employees.getTotalPages());
        return "employee/index";
    }

    @GetMapping("/{id}")
    public String show(Model model, @PathVariable Long id) {
        model.addAttribute("title", "Detail Employee");
        Employee employee = employeeDataSource.getById(id);

        model.addAttribute("employee", employee);
        return "employee/show";
    }

    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("title", "Create Employee");

        return "employee/form";
    }

    @PostMapping("")
    public String store(Model model, @ModelAttribute("employee") Employee employee) {
        try {
            employeeDataSource.insert(employee);
            model.addAttribute("message", "Employee has been created");
            return "redirect:/employee";
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            model.addAttribute("errorMessage", errorMessage);

            return "employee/create";
        }
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable Long id) {
        model.addAttribute("title", "Edit Employee");
        Employee employee = employeeDataSource.getById(id);

        model.addAttribute("employee", employee);
        return "employee/form";
    }

    @PutMapping("/{id}")
    public String update(Model model, @PathVariable Long id, @ModelAttribute("employee") Employee employee) {
        try {
            employeeDataSource.update(employee, id);
            model.addAttribute("message", "Employee has been updated");
            return "redirect:/employee";
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            model.addAttribute("errorMessage", errorMessage);

            return "employee/form";
        }
    }

    @DeleteMapping("/{id}")
    public String destroy(Model model, @PathVariable Long id) {
        try {
            employeeDataSource.deleteById(id);
            model.addAttribute("message", "Employee has been deleted");
            return "redirect:/employee";
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            model.addAttribute("errorMessage", errorMessage);

            return "employee";
        }
    }
}
