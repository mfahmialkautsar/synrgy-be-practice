package com.example.web.controller;

import com.example.web.entity.table.EmployeeTraining;
import com.example.web.repository.employeetraining.EmployeeTrainingDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1/employee-training")
public class EmployeeTrainingController {

    @Autowired
    private EmployeeTrainingDataSource employeeTrainingDataSource;

    @PostMapping("")
    public ResponseEntity<Object> addEmployee(@RequestBody Map<String, String> employeeTrainingBody) {
        Map<String, Object> res = new HashMap<>();
        try {
            EmployeeTraining employeeTraining = employeeTrainingDataSource.insert(employeeTrainingBody);

            res.put("status", "success");
            res.put("results", employeeTraining);
        } catch (Exception e) {
            res.put("status", "fail");
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }

        return new ResponseEntity<>(res, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateEmployeeTraining(@PathVariable Long id, @RequestBody Map<String, String> employeeTrainingBody) {
        Map<String, Object> res = new HashMap<>();
        try {
            employeeTrainingDataSource.update(id, employeeTrainingBody);
            res.put("status", "success");
        } catch (Exception e) {
            res.put("status", "fail");
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }

        return ResponseEntity.ok(res);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteEmployeeTraining(@PathVariable Long id) {
        Map<String, Object> res = new HashMap<>();
        try {
            employeeTrainingDataSource.delete(id);
            res.put("status", "success");
        } catch (Exception e) {
            res.put("status", "fail");
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }

        return ResponseEntity.ok(res);
    }
}
