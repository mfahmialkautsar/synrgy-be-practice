package com.example.web.controller.external;

import com.example.web.entity.table.Employee;
import com.example.web.exception.ItemNotFoundException;
import com.example.web.repository.rest.employee.EmployeeApiDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/employee")
public class EmployeeApiController {

    @Autowired
    EmployeeApiDataSource employeeApiDataSource;

    @PostMapping("")
    public ResponseEntity<Object> addEmployee(@RequestBody Employee employee) {
        Object insertedEmployee = employeeApiDataSource.insert(employee);
        Map<String, Object> res = new HashMap<>();
        res.put("message", "success");
        res.put("result", insertedEmployee);
        return new ResponseEntity<>(res, HttpStatus.CREATED);
    }

    @GetMapping("")
    public ResponseEntity<Object> getEmployees(@RequestParam(required = false) Integer page, @RequestParam(required = false) String name, @RequestParam(required = false) Integer status) {
        Map<String, Object> employeesResult = employeeApiDataSource.getAll(page, name, status);
        Map<String, Object> res = new HashMap<>();

        res.put("message", "success");
        res.put("page", employeesResult.get("page"));
        res.put("results", employeesResult.get("results"));
        res.put("total_results", employeesResult.get("total_results"));
        res.put("total_pages", employeesResult.get("total_pages"));
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getEmployeeById(@PathVariable Long id) {
        try {
            Object employee = employeeApiDataSource.getById(id);

            if (employee == null) throw new ItemNotFoundException(String.format("id-%s", id));

            Map<String, Object> res = new HashMap<>();
            res.put("message", "success");
            res.put("result", employee);
            return ResponseEntity.ok(res);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateEmployee(@RequestBody Employee employee, @PathVariable Long id) {
        try {
            Object employeeResult = employeeApiDataSource.update(employee, id);

            if (employeeResult == null) return ResponseEntity.notFound().build();

            employee.setId(id);

            Map<String, Object> res = new HashMap<>();
            res.put("message", "success");
            res.put("result", employee);

            return ResponseEntity.ok(res);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteEmployeeById(@PathVariable Long id) {
        try {
            employeeApiDataSource.deleteById(id);
            Map<String, Object> res = new HashMap<>();
            res.put("message", "success");

            return ResponseEntity.ok(res);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.notFound().build();
        }
    }
}
