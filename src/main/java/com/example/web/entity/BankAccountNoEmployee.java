package com.example.web.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
public class BankAccountNoEmployee implements Serializable {
    private String name;
    private String type;
    private Long number;
}
