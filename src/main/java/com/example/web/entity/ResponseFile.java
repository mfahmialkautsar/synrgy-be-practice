package com.example.web.entity;

import lombok.Data;

import javax.persistence.Lob;

@Data
public class ResponseFile {
    private String name, url, type;
    private long size;

    @Lob
    private byte[] data;

    public ResponseFile(String name, String url, String type, long size) {
        this.name = name;
        this.url = url;
        this.type = type;
        this.size = size;
    }
}
