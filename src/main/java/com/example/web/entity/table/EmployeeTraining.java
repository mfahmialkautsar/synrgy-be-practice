package com.example.web.entity.table;

import com.example.web.entity.table.extension.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "employee_training")
@Where(clause = "deleted_at IS null")
public class EmployeeTraining extends Timestamp implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "id", nullable = false)
    private Employee employee;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "training_id", referencedColumnName = "id", nullable = false)
    private Training training;

    @Column(name = "date", nullable = false)
    private Date date;
}
