package com.example.web.entity.table;

import com.example.web.entity.table.extension.Timestamp;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "employee")
public class Employee extends Timestamp implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 45)
    private String name;

    @Column(name = "gender", nullable = false, length = 11)
    private String gender;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "birth", nullable = false)
    private Date birth;

    @Column(name = "address")
    private String address;

    @Column(name = "picture_path")
    private String picturePath;

    @Column(name = "status", length = 2)
    private int status = 1;

    @OneToOne(mappedBy = "employee")
    private EmployeeDetail employeeDetail;

    @OneToMany(mappedBy = "employee")
    private Set<BankAccount> bankAccounts;

    @OneToMany(mappedBy = "employee")
    private Set<EmployeeTraining> employeeTrainings;
}
