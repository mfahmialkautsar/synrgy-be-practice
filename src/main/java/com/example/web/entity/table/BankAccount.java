package com.example.web.entity.table;

import com.example.web.entity.table.extension.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "bank_account")
@Where(clause = "deleted_at is null")
public class BankAccount extends Timestamp implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "number", nullable = false)
    private Long number;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "id", nullable = false)
    private Employee employee;

    public BankAccount() {
    }

    public BankAccount(String name, String type, Long number, Employee employee) {
        this.name = name;
        this.type = type;
        this.number = number;
        this.employee = employee;
    }
}
