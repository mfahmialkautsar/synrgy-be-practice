package com.example.web.entity.table;

import com.example.web.entity.table.extension.Timestamp;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "training")
@Where(clause = "deleted_at IS null")
public class Training extends Timestamp implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "topic", nullable = false)
    private String topic;

    @Column(name = "trainer", nullable = false)
    private String trainer;

    @OneToMany(mappedBy = "training")
    private Set<EmployeeTraining> employeeTrainings;
}
