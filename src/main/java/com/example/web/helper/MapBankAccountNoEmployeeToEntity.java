package com.example.web.helper;

import com.example.web.entity.BankAccountNoEmployee;
import com.example.web.entity.table.BankAccount;
import com.example.web.entity.table.Employee;

public class MapBankAccountNoEmployeeToEntity {
    private static volatile MapBankAccountNoEmployeeToEntity INSTANCE = null;

    public static MapBankAccountNoEmployeeToEntity getInstance() {
        if (INSTANCE == null) {
            synchronized (MapBankAccountNoEmployeeToEntity.class) {
                if (INSTANCE == null) {
                    INSTANCE = new MapBankAccountNoEmployeeToEntity();
                }
            }
        }
        return INSTANCE;
    }

    public BankAccount map(BankAccountNoEmployee bankAccountNoEmployee, Employee employee) {
        return new BankAccount(
                bankAccountNoEmployee.getName(),
                bankAccountNoEmployee.getType(),
                bankAccountNoEmployee.getNumber(),
                employee
        );
    }
}
