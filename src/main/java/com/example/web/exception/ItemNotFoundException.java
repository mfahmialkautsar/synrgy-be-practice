package com.example.web.exception;

public class ItemNotFoundException extends RuntimeException {
    public ItemNotFoundException(String exception) {
        super(exception);
    }
}
