package com.example.web.repository.employeetraining;

import com.example.web.entity.table.EmployeeTraining;
import com.example.web.entity.table.Training;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface EmployeeTrainingDao extends JpaRepository<EmployeeTraining, Long> {
    Page<EmployeeTraining> findByTraining(Pageable pageable, Training training);

    @Transactional
    @Modifying
    @Query("UPDATE EmployeeTraining SET deleted_at = CURRENT_TIMESTAMP WHERE id = :id")
    void softDeleteById(@Param("id") Long id);
}
