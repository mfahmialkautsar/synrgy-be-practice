package com.example.web.repository.employeetraining;

import com.example.web.entity.table.Employee;
import com.example.web.entity.table.EmployeeTraining;
import com.example.web.entity.table.Training;
import com.example.web.repository.employee.EmployeeDao;
import com.example.web.repository.training.TrainingDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

@Service
public class EmployeeTrainingRepository implements EmployeeTrainingDataSource {

    @Autowired
    private EmployeeTrainingDao employeeTrainingDao;

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private TrainingDao trainingDao;

    @Override
    public EmployeeTraining insert(Map<String, String> employeeTrainingBody) {
        String employeeId = employeeTrainingBody.get("employee_id");
        String trainingId = employeeTrainingBody.get("training_id");
        String dateString = employeeTrainingBody.get("date");
        if (employeeId == null || trainingId == null || dateString == null) throw new Error("400");

        Optional<Employee> employee = employeeDao.findById(Long.valueOf(employeeId));
        Optional<Training> training = trainingDao.findById(Long.valueOf(trainingId));
        Date date;
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = dateFormat.parse(dateString);
        } catch (Exception e) {
            throw new Error(e);
        }

        if (!employee.isPresent() || !training.isPresent()) throw new Error("404");

        EmployeeTraining employeeTraining = new EmployeeTraining();
        employeeTraining.setEmployee(employee.get());
        employeeTraining.setTraining(training.get());
        employeeTraining.setDate(date);

        return employeeTrainingDao.save(employeeTraining);
    }

    @Override
    public Page<EmployeeTraining> getEmployeeTrainingsByTrainingId(Long trainingId, Integer page) {
        if (page == null || page < 1) page = 1;

        Optional<Training> training = trainingDao.findById(trainingId);
        if (!training.isPresent()) throw new Error("404");

        Pageable pageable = PageRequest.of(page, 5);
        return employeeTrainingDao.findByTraining(pageable, training.get());
    }

    @Override
    public Optional<EmployeeTraining> getById(Long id) {
        return employeeTrainingDao.findById(id);
    }

    @Override
    public void update(Long id, Map<String, String> employeeTrainingBody) {
        String employeeId = employeeTrainingBody.get("employee_id");
        String trainingId = employeeTrainingBody.get("training_id");
        String dateString = employeeTrainingBody.get("date");
        if (employeeId == null || trainingId == null || dateString == null) throw new Error("400");

        Optional<EmployeeTraining> employeeTraining = employeeTrainingDao.findById(id);
        if (!employeeTraining.isPresent()) throw new Error("404");

        Optional<Employee> employee = employeeDao.findById(Long.valueOf(employeeId));
        Optional<Training> training = trainingDao.findById(Long.valueOf(trainingId));
        Date date;
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = dateFormat.parse(dateString);
        } catch (Exception e) {
            throw new Error(e);
        }

        if (!employee.isPresent() || !training.isPresent()) throw new Error("404");

        employeeTraining.get().setEmployee(employee.get());
        employeeTraining.get().setTraining(training.get());
        employeeTraining.get().setDate(date);
        employeeTrainingDao.save(employeeTraining.get());
    }

    @Override
    public void delete(Long id) {
        Optional<EmployeeTraining> employeeTraining = employeeTrainingDao.findById(id);
        if (!employeeTraining.isPresent()) throw new Error("404");

        employeeTrainingDao.softDeleteById(id);
    }
}
