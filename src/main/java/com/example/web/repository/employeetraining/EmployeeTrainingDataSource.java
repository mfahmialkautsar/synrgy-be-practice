package com.example.web.repository.employeetraining;

import com.example.web.entity.table.EmployeeTraining;
import org.springframework.data.domain.Page;
import org.springframework.lang.Nullable;

import java.util.Map;
import java.util.Optional;

public interface EmployeeTrainingDataSource {
    EmployeeTraining insert(Map<String, String> employeeTrainingBody);

    Page<EmployeeTraining> getEmployeeTrainingsByTrainingId(Long trainingId, @Nullable Integer page);

    Optional<EmployeeTraining> getById(Long id);

    void update(Long id, Map<String, String> employeeTrainingBody);

    void delete(Long id);
}
