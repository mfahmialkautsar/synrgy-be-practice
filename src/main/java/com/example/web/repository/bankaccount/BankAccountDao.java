package com.example.web.repository.bankaccount;

import com.example.web.entity.table.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface BankAccountDao extends JpaRepository<BankAccount, Long> {
    List<BankAccount> findByEmployeeIdAndDeletedAtIsNull(Long id);

    Optional<BankAccount> findOneByIdAndEmployeeIdAndDeletedAtIsNull(Long id, Long employeeId);

    @Transactional
    @Modifying
    @Query("UPDATE BankAccount SET deleted_at = CURRENT_TIMESTAMP WHERE employee_id = :employeeId AND id = :id")
    void softDeleteById(@Param("employeeId") Long employeeId, @Param("id") Long id);
}
