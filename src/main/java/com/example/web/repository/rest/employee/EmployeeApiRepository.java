package com.example.web.repository.rest.employee;

import com.example.web.entity.table.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

@Service
public class EmployeeApiRepository implements EmployeeApiDataSource {

    private static volatile RestTemplate REST_TEMPLATE;
    private static String URL = "http://localhost:8080/employee";

    public static RestTemplate getRestTemplate() {
        if (REST_TEMPLATE == null) {
            synchronized (RestTemplate.class) {
                if (REST_TEMPLATE == null) {
                    REST_TEMPLATE = new RestTemplate();
                }
            }
        }
        return REST_TEMPLATE;
    }

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    private static String getURL() {
        return URL;
    }

    @Override
    public Object insert(Employee employee) {
        return getRestTemplate().postForObject(getURL(), employee, Map.class).get("result");
    }

    @Override
    public Map<String, Object> getAll(Integer page, String name, Integer status) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getURL())
                .queryParam("page", page)
                .queryParam("name", name)
                .queryParam("status", status);
        return getRestTemplate().getForObject(builder.toUriString(), Map.class);
    }

    @Override
    public Object getById(Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getURL())
                .path(String.format("/%d", id));
        return getRestTemplate().getForObject(builder.toUriString(), Map.class).get("result");
    }

    @Override
    public Object update(Employee employee, Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getURL())
                .path(String.format("/%d", id));
        HttpEntity<Employee> employeeHttpEntity = new HttpEntity<>(employee);
        return getRestTemplate().exchange(builder.toUriString(), HttpMethod.PUT, employeeHttpEntity, Map.class).getBody().get("result");
    }

    @Override
    public void deleteById(Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getURL())
                .path(String.format("/%d", id));
        getRestTemplate().delete(builder.toUriString());
    }
}

