package com.example.web.service;

import com.example.web.configuration.FileStorageProperties;
import com.example.web.entity.ResponseFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.Objects;

@Service
public class FileStorageService {
    private final Path fileStorageLocation;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();

            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new RuntimeException("File not found" + fileName);
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public ResponseFile store(MultipartFile file) throws IOException {
        if (!Objects.equals(file.getContentType(), "image/png")
                && !Objects.equals(file.getContentType(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document"))
            throw new IOException(file.getContentType());

        Long millis = (new Date()).getTime();
        String generatedName = millis + file.getOriginalFilename();
        String filePath = this.fileStorageLocation + "/" + generatedName;
        Path path = Paths.get(filePath);

        try {
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IOException(e);
        }

        return new ResponseFile(file.getOriginalFilename(), generatedName, file.getContentType(), file.getSize());
    }
}
