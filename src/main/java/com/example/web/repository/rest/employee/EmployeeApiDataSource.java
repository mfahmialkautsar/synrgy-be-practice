package com.example.web.repository.rest.employee;

import com.example.web.entity.table.Employee;
import org.springframework.lang.Nullable;

import java.util.Map;

public interface EmployeeApiDataSource {
    Object insert(Employee employee);
    Map<String, Object> getAll(@Nullable Integer page, @Nullable String name, @Nullable Integer status);
    Object getById(Long id);
    Object update(Employee employee, Long id);
    void deleteById(Long id);
}
