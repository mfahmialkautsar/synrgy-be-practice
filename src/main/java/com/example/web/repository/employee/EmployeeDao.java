package com.example.web.repository.employee;

import com.example.web.entity.table.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface EmployeeDao extends JpaRepository<Employee, Long> {
    @Transactional
    @Modifying
    @Query("UPDATE Employee SET status = 2, deleted_at = CURRENT_TIMESTAMP WHERE id = :id")
    void softDeleteById(@Param("id") Long id);

    Page<Employee> findByStatusAndNameIgnoreCaseContainingOrderByName(Pageable pageable, Integer status, String name);
}
