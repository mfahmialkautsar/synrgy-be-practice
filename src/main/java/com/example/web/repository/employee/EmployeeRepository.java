package com.example.web.repository.employee;

import com.example.web.entity.table.Employee;
import com.example.web.entity.table.EmployeeDetail;
import com.example.web.exception.ItemNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeRepository implements EmployeeDataSource, EmployeeDetailDataSource {

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private EmployeeDetailDao employeeDetailDao;

    @Override
    public Employee insert(Employee employee) {
        return employeeDao.save(employee);
    }

    @Override
    public Page<Employee> getAll(Integer page, String name, Integer status) {
        Pageable pageable = PageRequest.of(page - 1, 5);
        return employeeDao.findByStatusAndNameIgnoreCaseContainingOrderByName(pageable, status, name);
    }

    @Override
    public Employee getById(Long id) {
        Optional<Employee> employee = employeeDao.findById(id);
        if (!employee.isPresent()) throw new ItemNotFoundException(String.format("id-%s", id));

        return employee.get();
    }

    @Override
    public Employee update(Employee employee, Long id) {
        Optional<Employee> employeeOptional = employeeDao.findById(id);

        if (!employeeOptional.isPresent()) throw new ItemNotFoundException(String.format("id-%s", id));

        employee.setId(id);
        return employeeDao.save(employee);
    }

    @Override
    public void deleteById(Long id) {
        employeeDao.softDeleteById(id);
    }

    @Override
    public EmployeeDetail insert(EmployeeDetail employeeDetail, Long id) {
        Employee employee = employeeDao.getById(id);
        if (employee.getEmployeeDetail() != null) throw new ItemNotFoundException(String.format("id-%s", id));

        employeeDetail.setEmployee(employee);
        return employeeDetailDao.save(employeeDetail);
    }

    @Override
    public EmployeeDetail update(EmployeeDetail employeeDetail, Long id) {
        Optional<EmployeeDetail> employeeDetailOptional = employeeDetailDao.findByEmployeeId(id);
        if (!employeeDetailOptional.isPresent()) throw new ItemNotFoundException(String.format("id-%s", id));

        employeeDetail.setId(employeeDetailOptional.get().getId());
        employeeDetail.setEmployee(employeeDao.getById(id));

        return employeeDetailDao.save(employeeDetail);
    }
}
