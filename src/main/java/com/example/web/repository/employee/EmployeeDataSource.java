package com.example.web.repository.employee;

import com.example.web.entity.table.Employee;
import org.springframework.data.domain.Page;
import org.springframework.lang.Nullable;

public interface EmployeeDataSource {
    Employee insert(Employee employee);
    Page<Employee> getAll(@Nullable Integer page, @Nullable String name, @Nullable Integer status);
    Employee getById(Long id);
    Employee update(Employee employee, Long id);
    void deleteById(Long id);
}
