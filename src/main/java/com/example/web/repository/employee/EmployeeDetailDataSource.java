package com.example.web.repository.employee;

import com.example.web.entity.table.EmployeeDetail;

public interface EmployeeDetailDataSource {
    EmployeeDetail insert(EmployeeDetail employeeDetail, Long id);

    EmployeeDetail update(EmployeeDetail employeeDetail, Long id);
}
