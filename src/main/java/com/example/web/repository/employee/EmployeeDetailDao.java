package com.example.web.repository.employee;

import com.example.web.entity.table.EmployeeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeDetailDao extends JpaRepository<EmployeeDetail, Long> {
    Optional<EmployeeDetail> findByEmployeeId(Long id);
}
