package com.example.web.repository.training;

import com.example.web.entity.table.Training;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TrainingRepository implements TrainingDataSource {

    @Autowired
    private TrainingDao trainingDao;

    @Override
    public Training insert(Training training) {
        return trainingDao.save(training);
    }

    @Override
    public Page<Training> getAll(Integer page) {
        if (page == null || page < 1) page = 1;
        Pageable pageable = PageRequest.of(page - 1, 5);
        return trainingDao.findAll(pageable);
    }

    @Override
    public Optional<Training> getById(Long id) {
        return trainingDao.findById(id);
    }

    @Override
    public void update(Training training, Long id) {
        Optional<Training> optionalTraining = trainingDao.findById(id);

        if (!optionalTraining.isPresent()) throw new Error("404");

        training.setId(id);
        trainingDao.save(training);
    }

    @Override
    public void delete(Long id) {
        Optional<Training> optionalTraining = trainingDao.findById(id);
        if (!optionalTraining.isPresent()) throw new Error("404");

        trainingDao.softDeleteById(id);
    }
}
