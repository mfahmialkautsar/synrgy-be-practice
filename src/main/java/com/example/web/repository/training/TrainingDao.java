package com.example.web.repository.training;

import com.example.web.entity.table.Training;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface TrainingDao extends JpaRepository<Training, Long> {
    @Transactional
    @Modifying
    @Query("UPDATE Training SET deleted_at = CURRENT_TIMESTAMP WHERE id = :id")
    void softDeleteById(@Param("id") Long id);
}
