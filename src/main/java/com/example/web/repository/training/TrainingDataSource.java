package com.example.web.repository.training;

import com.example.web.entity.table.Training;
import org.springframework.data.domain.Page;
import org.springframework.lang.Nullable;

import java.util.Optional;

public interface TrainingDataSource {
    Training insert(Training training);

    Page<Training> getAll(@Nullable Integer page);

    Optional<Training> getById(Long id);

    void update(Training training, Long id);

    void delete(Long id);
}
